---
title: Lettre d'information n°2
subtitle:
date: 2021-04-15T10:32:38-02:00
comments: false
author: Eric Couillard
---


## Changement de logo

![Nouveau logo académique](/img/logoAC_RENNES.jpg "Clic droit - Enregistrer sous...")

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquam arcu. Vivamus leo sem, fringilla ac dolor sed, fringilla aliquet risus. Nam eget elit turpis. Aenean sagittis enim leo, a mattis leo bibendum molestie. Nullam in magna vestibulum, mattis risus quis, accumsan lacus. Nulla eget eros dignissim, fermentum erat vel, ultrices eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam magna eros, tempor non porta vel, ultrices vel lorem. Suspendisse nibh nibh, vulputate sit amet aliquet a, sollicitudin id erat. Etiam dignissim accumsan turpis vulputate euismod. Proin fringilla feugiat justo.

## Migration vers SPIP 3.2.9

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque at aliquam arcu. Vivamus leo sem, fringilla ac dolor sed, fringilla aliquet risus. Nam eget elit turpis. Aenean sagittis enim leo, a mattis leo bibendum molestie. Nullam in magna vestibulum, mattis risus quis, accumsan lacus. Nulla eget eros dignissim, fermentum erat vel, ultrices eros. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam magna eros, tempor non porta vel, ultrices vel lorem. Suspendisse nibh nibh, vulputate sit amet aliquet a, sollicitudin id erat. Etiam dignissim accumsan turpis vulputate euismod. Proin fringilla feugiat justo.
